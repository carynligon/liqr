import os
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from src.app import create_app, db
from src.models.BottleModel import BottleModel
from src.models.UserModel import UserModel

env_name = os.getenv('FLASK_ENV')
app = create_app(env_name)

migrate = Migrate(app=app, db=db, user=UserModel, bottle=BottleModel)

manager = Manager(app=app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
  with app.app_context():
    # cursor = db.engine.raw_connection().cursor()
    # cursor.execute("select schema_name from information_schema.schemata")
    # results = list(cursor.fetchall())
    print(db.engine.url)
    try:
      manager.run()
    except Exception as e:
      print('e', e)