from flask import request, g, Blueprint, json, Response
from marshmallow import ValidationError
from ..shared.Authentication import Auth
from ..models.BottleModel import BottleModel, BottleSchema

bottle_api = Blueprint('bottle_api', __name__)
bottle_schema = BottleSchema()


@bottle_api.route('/', methods=['POST'])
@Auth.auth_required
def create():
  """
  Create Bottle Function
  """
  req_data = request.get_json()
  req_data['owner_id'] = g.user.get('id')
  try:
    data = bottle_schema.load(req_data)
  except ValidationError as err:
    return custom_response(err.messages, 400)
  post = BottleModel(data)
  post.save()
  # try:
  #   data = bottle_schema.dump(post).data
  # except ValidationError as err:
  #   return custom_response(err.messages, 400)

  return custom_response(data, 201)


def custom_response(res, status_code):
  """
  Custom Response Function
  """
  return Response(
    mimetype="application/json",
    response=json.dumps(res),
    status=status_code
  )
