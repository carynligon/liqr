from marshmallow import fields, Schema
import datetime
from . import db

class BottleModel(db.Model):
  """
  Bottle Model
  """

  __tablename__ = 'bottles'

  id = db.Column(db.Integer, primary_key=True)
  product = db.Column(db.String(128), nullable=False)
  description = db.Column(db.Text, nullable=False)
  fullness = db.Column(db.Integer, nullable=True)
  owner_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
  created_at = db.Column(db.DateTime)
  modified_at = db.Column(db.DateTime)

  def __init__(self, data):
    self.product = data.get('product')
    self.description = data.get('description')
    self.fullness = data.get('fullness')
    self.owner_id = data.get('owner_id')
    self.created_at = datetime.datetime.utcnow()
    self.modified_at = datetime.datetime.utcnow()

  def save(self):
    db.session.add(self)
    db.session.commit()

  def update(self, data):
    for key, item in data.items():
      setattr(self, key, item)
    self.modified_at = datetime.datetime.utcnow()
    db.session.commit()

  def delete(self):
    db.session.delete(self)
    de.session.commit()
  
  @staticmethod
  def get_all_bottles():
    return BottleModel.query.all()
  
  @staticmethod
  def get_one_bottle(id):
    return BottleModel.query.get(id)

  def __repr__(self):
    return '<id {}>'.format(self.id)

class BottleSchema(Schema):
  """
  Bottle Schema
  """
  id = fields.Int(dump_only=True)
  product = fields.Str(required=True)
  description = fields.Str(required=True)
  fullness = fields.Int(required=False)
  owner_id = fields.Int(required=True)
  created_at = fields.DateTime(dump_only=True)
  modified_at = fields.DateTime(dump_only=True)