from flask import Flask
from flask_cors import CORS

from .config import app_config
from .models import db, bcrypt
from .views.UserView import user_api as user_blueprint
from .views.BottleView import bottle_api as bottle_blueprint

def create_app(env_name):
  """
  Create app
  """
  
  # app initiliazation
  app = Flask(__name__)

  app.config.from_object(app_config[env_name])

  bcrypt.init_app(app)

  db.init_app(app)
  CORS(app)

  app.register_blueprint(user_blueprint, url_prefix='/api/v1/users')
  app.register_blueprint(bottle_blueprint, url_prefix='/api/v1/bottles')

  @app.route('/', methods=['GET'])
  def index():

    return 'Congratulations! Your first endpoint is workin'

  return app